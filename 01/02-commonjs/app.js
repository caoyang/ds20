/* 一般定义 app.js 文件，主要组织其他的模块进行业务开发 */

var m1 = require('./m1.js');
var m2 = require('./m2.js');
// 使用路径+文件方式导入其他的模块 m1.js 我们需要使用一个变量来接收导入模块的返回值，返回值是一个对象，该对象就是m1模块里面暴露出来的成员

console.log('m2的成员', m2);
console.log('m2的 sub 的使用', m2.sub(12, 2));

console.log('m1的成员', m1);
console.log(m1.username);
console.log(m1.number);
console.log(m1.addFn(12, 23));

