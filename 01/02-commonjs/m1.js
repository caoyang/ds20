var name = 'm1';
var number1 = 12;

function add(a, b) {
    return a + b;
}

// 方式1：导出
// exports 是 nodejs 里面的一个关键字 定义导出的成员
// exporst 是一个对象，定义属性就是导出的名称（用户可以自定义，一般都和导出的名称一致），属性值就是导出的成员
// exports.username = name;
// exports.number = number1;
// exports.addFn1 = add;

// 方式2：导出 还可以使用
module.exports = {
    username: name,
    number: number1,
    addFn: add
};

// 注意事项，上面两种导出不要混着使用，如果混着使用，则以 module.exports 为准


