/*nodejs 环境下，js文件里面可以写什么样的代码？*/


/* nodejs 组成：
*  1. ECMAScript: 指的是javascript的基础语法 var for + -  function .... es6
*  2. 内置核心模块：nodejs 内部提供很多的功能代码块，功能代码块可以快速方便开发者实现相关的功能（1. 网络操作，提供web服务 2. 文件的读取和写入）
*  3. 编程者自己编写的代码（遵循 javascript的语法（ECMAScript） 和 nodejs规范（commonjs规范）进行编写）
* */

/*
* nodejs 的环境下，是不能写之前前端开发里面的 dom和bom相关的代码
* document.getElementById('box').onclick = function (ev) {
    console.log(ev);
 }
因为 nodejs 是做后端开发，运行在服务器上面的，是没有 web前端的概念的，所以不能做 dom 和 bom 相关的操作。
* */

/*1. 变量的声明 2. 四则运算 3. 流程控制器 4. 函数 5. 面向对象 */
var number1 = 12;
var number2 = 23;

var rs = number1 + number2;

// 要输出内容
console.log(rs);// console.log(待输出的信息) 在控制台（黑窗口 dos）进行打印输出

for (let i = 0; i < 10; i++) {
    console.log('hi nodejs!');
}

function add(a, b) {
    return a + b;
}

var c = add(number1, number2);
console.log('c=', c);

function People(name) {
    this.name = name;

    this.say = function () {
        console.log('my name is ' + this.name);
    }
}

var p1 = new People('andy');
console.log(p1.name);
p1.say();