# ds20仓库简介
> 航海学院电商20级移动开发 代码仓库

# 腾讯课堂上课地址
> https://ke.qq.com/webcourse/4344124/104508742#from=800021724&lite=1&live=1


# 仓库地址
> https://gitee.com/caoyang/ds20.git


# 网盘资料地址
链接：https://pan.baidu.com/s/1m-s_Of0wmSmykvBPsSwd3A 

提取码：lj0u 

# 课程目标
1. nodejs 的学习
2. vuejs 的学习
3. 掌握前后端分离项目开发


# 编程工具
+ webstorm
+ vscode
+ hb
 