/**
 * 在日常开发中，我们会把常用的一些功能封装为工具库，单独提取为一个js文件。
 * 在需要的页面引入该文件，然后在使用对应的函数即可。
 *
 * 第一行是函数的功能描述：这种格式的注释一般我们叫做函数特有的注释风格，用来给函数做解释
 *
 * param 开头的是函数的参数的解释
 *
 * @param n {number 参数的类型} 范围的起始值
 * @param m {number 参数的类型} 范围的结束值
 *
 *returns 定义函数的返回值的解释
 * @returns {number 返回值的类型}
 */
function getRandomFromNtoM(n, m) {
    return Math.ceil(n + Math.random() * (m - n));
}
